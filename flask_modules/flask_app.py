from flask import Flask,request,jsonify,render_template   # Flask=class and is getting imported from flask module
import logging
import requests

# Creating object of Flask. Basicallyit starts the flask app
app=Flask(__name__)

browser_post=[{
        'city1':'Bihar',
        'city2':'Delhi',
        'city3':'Maharashtra',
        'city4':'Chattisgarh',
        'city5':'TamilNadu',
        'city6':'Andhra Pradesh'}]

logging.basicConfig(filename="F:\\python_flask_module\\flask_modules\\logger\\module.log", level=logging.DEBUG, format='(asctime)s %(levelname)s-%(message)s')

#Home page
@app.route("/home")
@app.route("/")
def home_page():
    logging.warning("logging the warning message")
    return render_template('home.html')

# Weather Page
@app.route("/weather")
def weather_page():
    logging.info("this is a weather page")
    return render_template('menu.html',post=browser_post)

#Contact Page
@app.route("/contact")
def contact_page():
    logging.info("this is a contact page")
    return render_template('contact.html')

#State Report page
#selection='null'
@app.route("/weatherReports",methods=['GET','POST'])
def WeatherReport_page():
    global selection
    selection = request.form.get('stateSelection') #using request object to get the value from the form
    if selection=='Bihar':
        return temperature_output()
    elif selection=='Delhi':
        return temperature_output()
    elif selection=='Maharashtra':
        return temperature_output()
    elif selection=='Chattisgarh':
        return temperature_output()

#Displays output of the temperature
def temperature_output():
    city = selection
    web_api = requests.get(
        'http://api.openweathermap.org/data/2.5/weather?q=' + city + '&appid=ee04e4e10ff5954c90e276b50bcb1d34')
    json = web_api.json()
    temp_k = float(json['main']['temp'])
    temp_celcius = float(temp_k - 273.15)
    return render_template('reports.html', state_temp=temp_celcius)

if __name__=='__main__':
    app.run(debug=True)


